ARG K_VERSION=5.6.58
ARG DISTRO=ubuntu
ARG DISTRO_VERSION=jammy

#https://hub.docker.com/r/runtimeverificationinc/kframework-k/tags
FROM runtimeverificationinc/kframework-k:${DISTRO}-${DISTRO_VERSION}-${K_VERSION}

RUN apt update
RUN apt upgrade -y

# Install pip for managing python requirements
RUN apt install -y python3-pip
RUN pip3 install -U pip
# Remove pip cache. We are not going to need it anymore
RUN rm -rf /root/.cache

# Send python output straight to terminal without bufferenig
ENV PYTHONUNBUFFERED 1

# Create custom user to avoid adding files as root.
ARG USERNAME=tutorial
# use your actual user and group id
# `id -u` and `id -g` 
ARG USER_UID=1000
ARG USER_GID=1000
# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME
USER $USER_UID:$USER_GID

RUN mkdir /home/tutorial/app
WORKDIR /home/tutorial/app
