# K Tutorial

Introduction to [K framework](https://kframework.org/).
For official tutorial checkout ["Other resources"](#other-resources) section.

## Requirements to run K framework

Here we'll use `Docker >= 20.10.17` to start Docker container with K framework.
For other installation methods checkout [official K framework installation page](https://github.com/runtimeverification/k/blob/master/k-distribution/INSTALL.md).

```bash
docker --version
```

## Start and enter a dev container

Build and start the container

```bash
docker compose up --build -d 
```

Check that the container is running:

```bash
docker ps
```

Enter the container:

```bash
./enter_dev.sh
```

## Other resources

* [Official K Framework tutorial](https://kframework.org/k-distribution/k-tutorial/1_basic/)
and the [corresponding github page](https://github.com/runtimeverification/k/tree/master/k-distribution/k-tutorial).

* [Builtin reference](https://kframework.org/k-distribution/include/kframework/builtin/domains/)
and the [corresponding github page](https://github.com/runtimeverification/k/blob/master/k-distribution/include/kframework/builtin/domains.md).

* [Matching Logic](http://www.matching-logic.org/)
